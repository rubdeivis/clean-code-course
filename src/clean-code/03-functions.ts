(() => {
  function getMovieById(movieId: string) {
    console.log({ movieId });
  }

  function getMovieCastByMovieId(id: string) {
    console.log({ id });
  }

  function getActorBioById(id: string) {
    console.log({ id });
  }

  interface Movie {
    cast: string[];
    description: string;
    rating: number;
    title: string;
  }

  function createMovie({ title, description, rating, cast }: Movie) {
    console.log({ title, description, rating, cast });
  }

  function checkFullName(fullName: string) {
    console.log({ fullName });
    return true;
  }

  function createActor(fullName: string, _birthdate: Date): boolean {
    if (checkFullName(fullName)) return false;

    console.log("Crear actor");
    return true;
  }

  // Continuar

  const getPayAmount = ({
    isDead = false,
    isSeparated = true,
    isRetired = false,
  }): number => {
    if (isDead) return 1500;

    if (isSeparated) return 1000;

    if (isRetired) return 500;

    return 4000;
  };
})();
